![alt text](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros/-/raw/master/icons/logo_long.svg)



# Vision Pipeline

[VPFR](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros)

[Wiki](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros/wikis/home)

---

# Single marker (aruco,opencv)

This is an implementation of a single marker detection with aruco pattern.  
OpenCV is used as the basis.  
There are several setting options.  

Details on this in the [wiki](https://gitlab.com/vision-pipeline-for-ros/algorithms/single_marker_aruco/-/wikis/home)
