"""
opencv_aruco
Copyright 2020 Vision Pipeline for ROS
Author: Fabian Kranewitter <fabiankranewitter@gmail.com>
SPDX-License-Identifier: MIT
"""

import cv2
import cv2.aruco as cvaruco
from vpfrsinglemarkerdetection.msg import SingleMarker
from geometry_msgs.msg import Pose, Point, Quaternion
import rospy
from cv_bridge import CvBridge, CvBridgeError
from algorithm_template import AlgorithmTemplate
from sensor_msgs.msg import CameraInfo, Image
import numpy as np
import tf


class OpenCvAruco(AlgorithmTemplate):
    def get_io_type(self):
        return Image, SingleMarker

    def on_enable(self):
        rospy.loginfo("[algorithm][opencv_aruco] init")
        self.__parameters = cvaruco.DetectorParameters_create()
        self.__bridge = CvBridge()
        self.__camera_matrix = None
        self.__dist_coeffs = None
        self.__ros_to_cv2_encoding = cv2.COLOR_BGR2RGB
        self.__marker_length = 0.1
        self.__aruco_dict = cvaruco.Dictionary_get(cvaruco.DICT_4X4_250)
        rospy.loginfo("[algorithm][opencv_aruco] end")

    def on_disable(self):
        pass

    def main(self, req):
        if self.__camera_matrix is not None and self.__dist_coeffs is not None:
            cv_image = self.__bridge.imgmsg_to_cv2(req)
            cv_image = cv2.cvtColor(cv_image, self.__ros_to_cv2_encoding)
            corners, ids, rejectedImgPoints = cvaruco.detectMarkers(
                cv_image, self.__aruco_dict, parameters=self.__parameters
            )
            if len(corners) > 0:
                rvecs, tvecs, _objPoints = cvaruco.estimatePoseSingleMarkers(
                    corners[0],
                    self.__marker_length,
                    self.__camera_matrix,
                    self.__dist_coeffs,
                )
                quat = tf.transformations.quaternion_from_euler(
                    float(rvecs.flat[0]),
                    float(rvecs.flat[1]),
                    float(rvecs.flat[2]),
                )
                quaternion = Quaternion(quat[0], quat[1], quat[2], quat[3])
                point = Point(tvecs.flat[0], tvecs.flat[1], tvecs.flat[2])
                pose = Pose(point, quaternion)
                return SingleMarker(ids.flat[0], pose)
            return SingleMarker(0, None)
        else:
            return SingleMarker(0,None)

    def _cb_getCameraInfo(self, info):
        self.__cam_info_sub.unregister()
        try:
            camera_matrix = info.K
            dist_coeffs = info.D
            self.__camera_matrix = np.reshape(camera_matrix, (3, 3))
            self.__dist_coeffs = np.reshape(dist_coeffs, (1, 5))
        except Exception as exc:
            rospy.logwarn(
                "[algorithm][opencv_aruco] can not convert camera_matrix :"+ str(exc)
            )

    def on_additional_data_change(self, data):
        if len(data) >= 1:
            self.__camera_settings_info_topic = data[0]
            rospy.loginfo(
                "[algorithm][opencv_aruco] read camera info from "
                + self.__camera_settings_info_topic
            )
            self.__cam_info_sub = rospy.Subscriber(
                self.__camera_settings_info_topic,
                CameraInfo,
                self._cb_getCameraInfo,
            )
        else:
            rospy.loginfo("[algorithm][opencv_aruco] no additional data")

    def on_config_change(self, item):
        if item is None:
            return

        ros_to_cv2_encoding = item.get("ros_to_cv2_encoding")
        if ros_to_cv2_encoding is not None:
            if ros_to_cv2_encoding is not None or ros_to_cv2_encoding >= 0:
                rospy.loginfo(
                    "[algorithm][opencv_aruco] ros_to_cv2_encoding set to '%s'",
                    ros_to_cv2_encoding,
                )
                self.__ros_to_cv2_encoding = ros_to_cv2_encoding
            else:
                rospy.logwarn(
                    "[algorithm][opencv_aruco] ros_to_cv2_encoding type '%s' not found",
                    ros_to_cv2_encoding,
                )

        dictionary_name = item.get("dictionarytype")
        if dictionary_name is not None and isinstance(dictionary_name, int):
            dictionarytype = cvaruco.Dictionary_get(dictionary_name)
            if dictionarytype is not None:
                rospy.loginfo(
                    "[algorithm][opencv_aruco] dictionarytype set to '%s'",
                    dictionary_name,
                )
                self.__aruco_dict = dictionarytype
            else:
                rospy.logwarn(
                    "[algorithm][opencv_aruco] dictionarytype '%s' not found",
                    dictionary_name,
                )

        marker_length = item.get("markerLength")
        if marker_length is not None:
            if isinstance(marker_length, float):
                rospy.loginfo(
                    "[algorithm][opencv_aruco] markerLength set to '%s'",
                    marker_length,
                )
                self.__marker_length = float(marker_length)
            else:
                rospy.logwarn(
                    "[algorithm][opencv_aruco] markerLength is not a float"
                )
